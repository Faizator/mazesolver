package com.faz.scaling;

/**
 * Created by FAZ on 24.06.2017.
 */

public interface ILogger {
    void log(String msg);
}
