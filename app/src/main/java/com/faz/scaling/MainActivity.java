package com.faz.scaling;

import android.app.ActivityManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    volatile int TRESHOLD = 128;
    int[][] map;

    @BindView(R.id.demoBtn) Button demoBtn;
    @BindView(R.id.photoBtn) Button photoBtn;
    @BindView(R.id.applyTresholdChangesBtn) Button applyTresholdChangesBtn;
    @BindView(R.id.tresholdSeekbar) SeekBar tresholdSeekbar;
    @BindView(R.id.processingView) View processingView;
    volatile Bitmap currentNotProcessedBitmap;
    volatile Bitmap currentProcessedBitmap;
    volatile boolean isProcessing;
    @BindView(R.id.tiv) TouchImageView tiv;
    @BindView(R.id.image) ImageView image;

    String currentPhotoPath;
    File photoFile;

    static final int REQUEST_TAKE_PHOTO = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Maze Solver");
        ButterKnife.bind(this);

        if( !Utils.checkPermissions(this.getApplicationContext()) ) {
            Utils.requestPermissionsIfNotSet(this);
        }

        tiv.setMaxZoom(1024f);
        tiv.setmOnTwoPointsSetListener(new TouchImageView.OnTwoPointsSetListener() {
            @Override
            public void onTwoPointsSet(final int x1, final int y1, final int x2, final int y2) {
                System.out.println(x1 + " : " + y1 + " :  "+ x2 + " : " + y2);
                synchronized (this) {
                    if( isProcessing ) {
                        Toast.makeText(MainActivity.this, "Изображение обрабатывается", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    isProcessing = true;
                    processingView.setVisibility(View.VISIBLE);
                }

                Observable.fromCallable(new Callable<int[][]>() {
                    @Override
                    public int[][] call() throws Exception {
                        int[][] copyMap = Utils.deepCopy(map);
                        WaveAlgorithm algorithm = new WaveAlgorithm();
                        int[][] path = algorithm.solve(new int[]{y1, x1}, new int[]{y2, x2}, copyMap);
                        return path;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<int[][]>() {
                    @Override
                    public void call(int[][] path) {
                        Utils.print2DArray(path);
                        tiv.setPath(path);
                        isProcessing = false;
                        processingView.setVisibility(View.GONE);
                    }
                });
            }
        });
        tiv.setTreshold(TRESHOLD);

        tresholdSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                applyTresholdChangesBtn.setVisibility(View.VISIBLE);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        demoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tiv.reset();
//                InputStream currentImageInputStream = getResources().openRawResource(R.raw.maze);
//                currentNotProcessedBitmap = Utils.decodeSampledBitmapFromStream(currentImageInputStream, 400);
                currentNotProcessedBitmap = Utils.decodeSampledBitmapFromResource(getResources(), R.raw.maze, 400);
                processImage();
            }
        });

        photoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        applyTresholdChangesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                synchronized (this) {
                    if( isProcessing ) {
                        Toast.makeText(MainActivity.this, "Изображение обрабатывается", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                TRESHOLD = tresholdSeekbar.getProgress();
                applyTresholdChangesBtn.setVisibility(View.INVISIBLE);
                processImage();
            }
        });
    }

    private boolean processImage() {
        synchronized (this) {
            if( isProcessing ) {
                Toast.makeText(this, "Изображение обрабатывается", Toast.LENGTH_SHORT).show();
                return false;
            }
            isProcessing = true;
            processingView.setVisibility(View.VISIBLE);
        }
        if( currentNotProcessedBitmap == null ) {
            Toast.makeText(this, "Изображение не задано", Toast.LENGTH_SHORT).show();
            isProcessing = false;
            processingView.setVisibility(View.GONE);
            return false;
        }

        Observable.just(currentNotProcessedBitmap)
        .map(new Func1<Bitmap, Bitmap>() {
            @Override
            public Bitmap call(Bitmap bitmap) {
                Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                map = new int[bitmap.getHeight()][bitmap.getWidth()];
                for( int y=0; y<bitmap.getHeight(); y++ ) {
                    for( int x=0; x<bitmap.getWidth(); x++ ) {
                        int pixel = bitmap.getPixel(x, y);
                        if( Color.red(pixel) < TRESHOLD || Color.blue(pixel) < TRESHOLD || Color.green(pixel) < TRESHOLD ) {
                            newBitmap.setPixel(x, y, Color.BLACK);
                            map[y][x] = -1;
                        } else {
                            newBitmap.setPixel(x, y, Color.WHITE);
                            map[y][x] = 0;
                        }
                    }
                }
//                bitmap.recycle();
                return newBitmap;
            }
        })
        .onErrorReturn(new Func1<Throwable, Bitmap>() {
            @Override
            public Bitmap call(Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                return null;
            }
        })
        .filter(new Func1<Bitmap, Boolean>() {
            @Override
            public Boolean call(Bitmap bitmap) {
                return bitmap != null;
            }
        })
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Action1<Bitmap>() {
            @Override
            public void call(Bitmap bitmap) {
                tiv.setBitmap(bitmap);
                isProcessing = false;
                processingView.setVisibility(View.GONE);
            }
        });

        return true;
    }



    private File createImageFile() throws IOException {
        String imageFileName = "SOME.jpg";
        File image = new File(Environment.getExternalStorageDirectory(),  imageFileName);
        image.delete();
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                Uri photoURI = Uri.fromFile(photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            currentNotProcessedBitmap = Utils.decodeSampledBitmapFromFile(currentPhotoPath, 400);
            image.setImageBitmap( currentNotProcessedBitmap );
            tiv.reset();
            processImage();
        } else {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }
    }
}
