package com.faz.scaling;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.io.File;
import java.io.InputStream;
import java.util.Arrays;

/**
 * Created by FAZ on 24.06.2017.
 */

public class Utils {
    public static int calculateInSampleSize(BitmapFactory.Options options, final int MAX_SIDE) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        float max = Math.max(height, width);
        int inSampleSize = 1;
        while( max > MAX_SIDE ) {
            max /= 2;
            inSampleSize *= 2;
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources resources, int resourceId, int MAX_SIDE) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, resourceId, options);
        options.inSampleSize = calculateInSampleSize(options, MAX_SIDE);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        System.out.println("INSAMPLE " + options.inSampleSize);
        System.out.println("INSAMPLE " + options.outHeight);
        System.out.println("INSAMPLE " + options.outWidth);
        return BitmapFactory.decodeResource(resources, resourceId, options);
    }

    public static Bitmap decodeSampledBitmapFromStream(InputStream imageStream, int MAX_SIDE) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(imageStream, null, options);
        options.inSampleSize = calculateInSampleSize(options, MAX_SIDE);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        System.out.println("INSAMPLE " + options.inSampleSize);
        System.out.println("INSAMPLE " + options.outHeight);
        System.out.println("INSAMPLE " + options.outWidth);
        return BitmapFactory.decodeStream(imageStream, null, options);
    }

    public static Bitmap decodeSampledBitmapFromFile(String filePath, int MAX_SIDE) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);
        options.inSampleSize = calculateInSampleSize(options, MAX_SIDE);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        System.out.println("INSAMPLE " + options.inSampleSize);
        System.out.println("INSAMPLE " + options.outHeight);
        System.out.println("INSAMPLE " + options.outWidth);
        return BitmapFactory.decodeFile(filePath, options);
    }

    public static int[][] deepCopy(int[][] original) {
        if (original == null) {
            return null;
        }

        final int[][] result = new int[original.length][];
        for (int i = 0; i < original.length; i++) {
            result[i] = Arrays.copyOf(original[i], original[i].length);
        }
        return result;
    }

    public static void print2DArray(int[][] arr) {
        print2DArray(arr, MLogger.getInstance());
    }
    public static void print2DArray(int[][] arr, ILogger logger) {
        if( arr == null ) {
            logger.log("Null");
            return;
        }
        for( int[] m : arr ) {
            logger.log(Arrays.toString(m));
        }
    }

    public static void requestPermissionsIfNotSet(Activity activity) {
        if( !checkPermissions(activity.getApplicationContext()) ) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{
                            Manifest.permission.INTERNET,
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1111);
        }
    }

    public static boolean checkPermissions(Context context) {
        boolean hasPermission = (ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        return hasPermission;
    }

}
