package com.faz.scaling;

/**
 * Created by FAZ on 24.06.2017.
 */
public class WaveAlgorithm implements IPathFindAlgorithm {

    @Override
    public int[][] solve(int[] start, int[] end, int[][] map) {
        int[][] copyMap = Utils.deepCopy(map);
        copyMap[start[0]][start[1]] = 1;
        boolean endReached = false;
        boolean anythingWasSet = true;
        while( !endReached && anythingWasSet ) {
            anythingWasSet = false;
            for (int i = 0; i < map.length; i++) {
                for (int j = 0; j < map[0].length; j++) {
                    anythingWasSet = setClosestCells(copyMap, i, j) || anythingWasSet;
                }
            }
            if( copyMap[end[0]][end[1]] > 0 ) {
                endReached = true;
            }
            System.out.println("ITERATION");
        }

        if( !endReached ) {
            return null;
        }
//        return copyMap;
//        System.out.println("---------------");
        Utils.print2DArray(copyMap);
        int[][] path = reconstructPath(copyMap, end[0], end[1]);
        return path;
    }

    public int[][] reconstructPath(int[][] map, int endI, int endJ) {
        int pathLength = map[endI][endJ];
        int[][] path = new int[pathLength][2];
        path[0] = new int[]{endI, endJ};
        for( int i=1; i<pathLength; i++ ) {
            path[i] = closestCell(map, path[i-1][0], path[i-1][1]);
        }
        return path;
    }

    public int[] closestCell(int[][] map, int i, int j) {
        if( map[i][j] == 1 ) {
            return new int[]{i,j};
        }
        for( int ii = i-1; ii<=i+1; ii++ ) {
            for( int jj = j-1; jj<=j+1; jj++ ) {
                if( ii >=0 && ii<map.length ) {
                    if( jj >=0 && jj<map[0].length ) {
                        if( map[ii][jj] == 0 || map[ii][jj] == -1 ) {
                            continue;
                        }
                        if( map[ii][jj] < map[i][j] ) {
                            return new int[]{ii,jj};
                        }
                    }
                }
            }
        }
        return null;
    }

    public boolean setClosestCells(int[][] map, int i, int j) {
        if( map[i][j] == -1 || map[i][j] == 0 ) {
            return false;
        }
        boolean anythingWasSet = false;
        int value = map[i][j];
        for( int ii = i-1; ii<=i+1; ii++ ) {
            for( int jj = j-1; jj<=j+1; jj++ ) {
                if( ii >=0 && ii<map.length ) {
                    if( jj >=0 && jj<map[0].length ) {
                        if( map[ii][jj] == 0 ) {
                            map[ii][jj] = value+1;
                            anythingWasSet = true;
                        } else if( map[ii][jj] > value+1 ) {
                            map[ii][jj] = value+1;
                            anythingWasSet = true;
                        }
                    }
                }
            }
        }
        return anythingWasSet;
    }
}
