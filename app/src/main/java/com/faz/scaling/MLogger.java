package com.faz.scaling;

import android.util.Log;

/**
 * Created by FAZ on 24.06.2017.
 */

public class MLogger implements ILogger {
    private static MLogger instance;

    @Override
    public void log(String msg) {
        Log.d("TAG", msg);
    }

    public static MLogger getInstance() {
        if( instance == null ) {
            instance = new MLogger();
        }
        return instance;
    }
}
