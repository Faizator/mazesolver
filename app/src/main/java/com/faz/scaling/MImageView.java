package com.faz.scaling;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by FAZ on 11.06.2017.
 */

public class MImageView extends View {
    private Paint mPaint = new Paint();
    private Bitmap mBitmap;
    private Matrix mDrawMatrix;

    public MImageView(Context context) {
        super(context);
    }

    public MImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mBitmap == null) {
            return; // couldn't resolve the URI
        }

        if (mDrawMatrix == null) {
            canvas.drawBitmap(mBitmap, 0, 0, mPaint);
        } else {
            final int saveCount = canvas.getSaveCount();
            canvas.save();

            if (mDrawMatrix != null) {
                canvas.concat(mDrawMatrix);
            }
            canvas.drawBitmap(mBitmap, 0, 0, mPaint);
            canvas.restoreToCount(saveCount);
        }
    }

    public void setMatrix(Matrix matrix) {
        this.mDrawMatrix = matrix;
        invalidate();
    }

    public void setBitmap(Bitmap bitmap) {
        this.mBitmap = bitmap;
        invalidate();
    }
}
