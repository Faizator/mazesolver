package com.faz.scaling;

/**
 * Created by FAZ on 24.06.2017.
 */

public class ConsoleLogger implements ILogger {
    @Override
    public void log(String msg) {
        System.out.println(msg);
    }
}
