package com.faz.scaling;

/**
 * Created by FAZ on 24.06.2017.
 */
public interface IPathFindAlgorithm {
    int[][] solve(int[] start, int[] end, int[][] map);
}
