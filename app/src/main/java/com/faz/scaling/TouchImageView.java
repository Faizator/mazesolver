package com.faz.scaling;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import java.util.ArrayList;

public class TouchImageView extends View {
    Matrix matrix;
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    static final int CLICK = 5;

    int mode = NONE;
    int DRAG_SLOPE = 5;

    PointF last = new PointF();
    PointF start = new PointF();
    float minScale = 0.1f;
    float maxScale = 3f;
    float[] m;

    float offsetX = 0;
    float offsetY = 0;
    float saveScale = 1f;

    int WIDTH = 0;
    int HEIGHT = 0;

    private Paint mPaint;
    private Bitmap mBitmap;
    private Bitmap[][] mTiles;
    private Matrix mDrawMatrix;


    ScaleGestureDetector mScaleDetector;
    OnTwoPointsSetListener mOnTwoPointsSetListener;

    Context context;

    ArrayList<Point> points = new ArrayList<>();
    Bitmap subsampledPart = null;
    int subsampledPartOffsetX = 0;
    int subsampledPartOffsetY = 0;

    public TouchImageView(Context context) {
        super(context);
        sharedConstructing(context);
    }

    public TouchImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        sharedConstructing(context);
    }

    float[] dst =new float[2];
    float[] src =new float[2];
    int TRESHOLD = 100;
    int[][] mPath;

    public void reset() {
        offsetX = 0;
        offsetY = 0;
        saveScale = 1f;
        points = new ArrayList<>();
        mPath = null;
        matrix = new Matrix();
        setMatrix(matrix);
    }

    private void sharedConstructing(Context context) {
        super.setClickable(true);
        this.context = context;
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
        matrix = new Matrix();
        m = new float[9];
        setMatrix(matrix);
        mPaint = new Paint();
        mPaint.setColor(Color.parseColor("#FF0000"));
        mPaint.setStrokeWidth(1);

        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                getParent().requestDisallowInterceptTouchEvent(true);
                mScaleDetector.onTouchEvent(event);
                PointF curr = new PointF(event.getX(), event.getY());
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        last.set(curr);
                        start.set(last);
                        mode = CLICK;
                        break;

                    case MotionEvent.ACTION_MOVE:
                        float deltaX = curr.x - last.x;
                        float deltaY = curr.y - last.y;
                        System.out.println("DELTAX " + deltaX);
                        if( mode == CLICK && (Math.abs(deltaX) > DRAG_SLOPE || Math.abs(deltaY) > DRAG_SLOPE) ) {
                            mode = DRAG;
                        }
                        if (mode == DRAG) {
                            matrix.postTranslate(deltaX, deltaY);
                            last.set(curr.x, curr.y);
                        }
                        break;

                    case MotionEvent.ACTION_UP:
                        if( mode == CLICK ) {
                            addPoint(curr.x, curr.y);
                        }
                        mode = NONE;
                        break;

                    case MotionEvent.ACTION_POINTER_UP:
                        mode = NONE;
                        break;
                }

                setMatrix(matrix);
                return true; // indicate event was handled
            }
        });
    }

    public void setMaxZoom(float x) {
        maxScale = x;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            mode = ZOOM;
            return true;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float mScaleFactor = detector.getScaleFactor();
            float origScale = saveScale;
            saveScale *= mScaleFactor;
            if (saveScale > maxScale) {
                saveScale = maxScale;
                mScaleFactor = maxScale / origScale;
            } else if (saveScale < minScale) {
                saveScale = minScale;
                mScaleFactor = minScale / origScale;
            }

            matrix.postScale(mScaleFactor, mScaleFactor, detector.getFocusX(), detector.getFocusY());
//            System.out.println("FOCUS " + detector.getFocusX() + ":" + detector.getFocusY());
            return true;
        }
    }

    public void setTreshold(int treshold) {
        TRESHOLD = treshold;
    }

    public float getOffsetX() {
        src[0] = 0;
        src[1] = 0;
        matrix.mapPoints(dst, src);
        return dst[0];
    }

    public float getOffsetY() {
        src[0] = 0;
        src[1] = 0;
        matrix.mapPoints(dst, src);
        return dst[1];
    }

    public float getScale() {
        return saveScale;
    }

    public void setmOnTwoPointsSetListener(OnTwoPointsSetListener listener) {
        this.mOnTwoPointsSetListener = listener;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mBitmap == null && mTiles == null) {
            return; // couldn't resolve the URI
        }

        final int saveCount = canvas.getSaveCount();
        canvas.save();

        if (mDrawMatrix != null) {
            canvas.concat(mDrawMatrix);
        }
        drawImage(canvas);
        drawPoints(canvas);
        drawPath(canvas);
        canvas.restoreToCount(saveCount);
    }

    private void drawImage(Canvas canvas) {
        if( mBitmap != null ) {
            canvas.drawBitmap(mBitmap, 0, 0, mPaint);
        }
        float localOffsetX = 0;
        float localOffsetY = 0;
        if( mTiles != null ) {
            for( int i=0; i<mTiles.length; i++ ) {
                for( int j=0; j<mTiles[i].length; j++ )  {
                    canvas.drawBitmap(mTiles[i][j], localOffsetX, localOffsetY, mPaint);
                    localOffsetX += mTiles[i][j].getWidth();
                }
                localOffsetX = 0;
                localOffsetY += mTiles[i][0].getHeight();
            }
        }
    }

    private void drawPoints(Canvas canvas) {
        for( Point point : points ) {
            canvas.drawCircle(point.x, point.y, 10f/saveScale, mPaint);
        }
    }

    private void drawPath(Canvas canvas) {
        if( mPath != null ) {
            for (int i=1; i<mPath.length; i++) {
//                canvas.drawCircle(point.x, point.y, 10f / saveScale, mPaint);
                int x1 = mPath[i-1][1];
                int y1 = mPath[i-1][0];
                int x2 = mPath[i][1];
                int y2 = mPath[i][0];
                canvas.drawLine(x1, y1, x2, y2, mPaint);
            }
        }
    }

    public void setMatrix(Matrix matrix) {
        this.mDrawMatrix = matrix;
        invalidate();
    }

    public void setBitmap(Bitmap bitmap) {
        this.mBitmap = bitmap;
        this.mTiles = null;
        invalidate();
    }

    public void setBitmaps(Bitmap[][] tiles) {
        this.mBitmap = null;
        this.mTiles = tiles;
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        WIDTH = w;
        HEIGHT = h;
    }

    public void addPoint(float x, float y) {
        x = (x - getOffsetX()) / saveScale;
        y = (y - getOffsetY()) / saveScale;

        if (x < 0 || y < 0 || x >= mBitmap.getWidth() || y >= mBitmap.getHeight()) {
            return;
        }

        int pixel = mBitmap.getPixel((int)x, (int)y);
        if (Color.red(pixel) < TRESHOLD || Color.blue(pixel) < TRESHOLD || Color.green(pixel) < TRESHOLD) {
            return;
        }

        if( points.size() >= 2 ) {
            points.clear();
            mPath = null;
//            points.remove(0);
        }
        points.add(new Point(
                (int)x, (int)y
        ));

        if( points.size() == 2 && mOnTwoPointsSetListener != null ) {
            mOnTwoPointsSetListener.onTwoPointsSet( points.get(0).x, points.get(0).y, points.get(1).x, points.get(1).y );
        }
    }

    public void setPath(int[][] path) {
        mPath = path;
        invalidate();
    }

    public interface OnTwoPointsSetListener {
        void onTwoPointsSet(int x1, int y1, int x2, int y2);
    }
}